package mx.fonyoutelecom.examen.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;
import mx.fonyoutelecom.examen.model.ConsultaAssignResponse;
import mx.fonyoutelecom.examen.model.ConsultaExamResponse;
import mx.fonyoutelecom.examen.model.ConsultaStudentsResponse;
import mx.fonyoutelecom.examen.model.ExamAssignment;
import mx.fonyoutelecom.examen.model.ResponseAnsweExam;
import mx.fonyoutelecom.examen.model.RpBase;
import mx.fonyoutelecom.examen.model.RqAnswerExam;
import mx.fonyoutelecom.examen.model.RqExam;
import mx.fonyoutelecom.examen.model.Student;
import mx.fonyoutelecom.examen.model.Status;
import mx.fonyoutelecom.examen.service.IExamenService;
import mx.fonyoutelecom.examen.utils.UtilsExam;

/**
 * Controlador que se encarga de recibir las peticiones http
 * 
 * @author IAV
 *
 */
@Slf4j
@RestController
@RequestMapping("/exam")
public class ExamenController {
	
	/** Inyeccion de dependencia de examen service */
	@Autowired
	private IExamenService examService;

	/**
	 * Metodo que se encarga de crear un examen
	 * 
	 * @param rqExam informacion del examen a crear
	 * @return ResponseEntity respuesta de la operacion
	 */
	@PostMapping(value = "/create-exam", produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RpBase> createExam(@Valid @RequestBody RqExam rqExam) {
		RpBase resp = new RpBase();
		log.info("Inicia creacion de examen");
		HttpStatus http = HttpStatus.CREATED;
		
		/** Se valida que el puntaje del examen sea 100 */
		if(rqExam.getQuestions().stream().mapToDouble(q -> q.getScore()).sum() == 100) {
			examService.createExam(rqExam);
			
			resp.setStatus(new Status());
			resp.getStatus().setCode("OK");
			resp.getStatus().setMessage("Examen guardado de manera exitosa");
		} else {
			resp.setStatus(new Status());
			resp.getStatus().setCode("ERR02");
			resp.getStatus().setMessage("El puntaje del examen debe de ser igual a 100");
			http = HttpStatus.BAD_REQUEST;
		}
		
		
		log.info("Fin creacion de examen");
		return new ResponseEntity<>(resp, http);
	}
	
	/**
	 * Metodo que se encarga de consultar los examenes creados
	 * 
	 * @return ResponseEntity
	 */
	@GetMapping(value = "/get-exams", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ConsultaExamResponse> getExam() {
		log.info("Inicia consulta de examenes");
		ConsultaExamResponse examResponse = new ConsultaExamResponse();
		examResponse.setExams(examService.getAllExam());
		examResponse.setStatus(new Status());
		examResponse.getStatus().setCode("OK");
		examResponse.getStatus().setMessage("Consulta de examenes exitosa");
		log.info("Fin consulta de examenes");
		return new ResponseEntity<ConsultaExamResponse>(examResponse, HttpStatus.OK);
	}
	
	/**
	 * Metodo que se encarga de recibir la peticion para la creacion de un estudiante
	 * 
	 * @param rqStudent datos del estudiante a crear
	 * @return ResponseEntity
	 */
	@PostMapping(value = "/create-student", produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RpBase> createStudent(@Valid @RequestBody Student rqStudent) {
		log.info("Inicia creacion de estudiante");
		RpBase resp = new RpBase();
		resp.setStatus(new Status());

		/** Se valida que la zona horaria sea valida */
		if(UtilsExam.isTimeZoneValid(rqStudent.getTimeZone())) {
			examService.createStudent(rqStudent);
			resp.getStatus().setCode("OK");
			resp.getStatus().setMessage("Se creo el estudiante de manera correcta");
		} else {
			resp.getStatus().setCode("ERR001");
			resp.getStatus().setMessage("La zona horaria no es valida");
		}
		log.info("Fin creacion de estudiante");
		return new ResponseEntity<>(resp, HttpStatus.CREATED);
	}
	
	/**
	 * Se consultan los estudiantes creados
	 * 
	 * @return ResponseEntity<ConsultaStudentsResponse> contiene la lista de estudiantes creados
	 */
	@GetMapping(value = "/get-student", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ConsultaStudentsResponse> getStudent() {
		log.info("Inicia creacion de estudiante");
		ConsultaStudentsResponse resp = new ConsultaStudentsResponse();
		
		resp = examService.getStudents();
		resp.setStatus(new Status());
		resp.getStatus().setCode("OK");
		resp.getStatus().setMessage("Consulta de estudiantes exitosa");
		
		log.info("Fin creacion de estudiante");
		return new ResponseEntity<>(resp, HttpStatus.CREATED);
	}
	
	/**
	 * Metodo que se utiliza para consultar las asignaciones de un estudiantes solo se regresan las asignaciones donde ya ha pasado la fecha asignada
	 * 
	 * @param id del estudiante
	 * @return ResponseEntity<ConsultaAssignResponse> contiene la lista de asginaciones
	 */
	@GetMapping(value = "/get-assign-by-student/{id_student}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ConsultaAssignResponse> getAssign(@PathVariable("id_student") int id) {
		log.info("Inicia consulta de asignacion por el Id del estudiante: " + id);
		ConsultaAssignResponse resp = new ConsultaAssignResponse();
		resp.setAssing(examService.getAssignmentByStudent(id));
		resp.setStatus(new Status());
		resp.getStatus().setCode("OK");
		resp.getStatus().setMessage("Consulta de estudiantes exitosa");
		
		log.info("Fin creacion de estudiante");
		return new ResponseEntity<>(resp, HttpStatus.CREATED);
	}
	
	/**
	 * Metodo que se encarga de recibir los datos para hacer la asignacion de un examen
	 * 
	 * @param examAssignment datos de la assignacion
	 * 
	 * @return ResponseEntity<RpBase>
	 */
	@PostMapping(value = "/assign-exam", produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RpBase> assignExam(@Valid @RequestBody ExamAssignment examAssignment) {
		log.info("Inicia asignacion de examen: " + examAssignment.toString());
		RpBase resp = new RpBase();
		resp.setStatus(new Status());
		
		examService.examAssignment(examAssignment);
		
		resp.getStatus().setCode("OK");
		resp.getStatus().setMessage("Asignacion de examen exitosa");
		
		
		log.info("Fin asignacion de examen");
		return new ResponseEntity<>(resp, HttpStatus.OK);
	}
	
	/**
	 * Metodo encargado de recibir los datos de las respuestas del examen
	 * 
	 * @param rqAnswerExam respuesta del examen
	 * @return ResponseEntity<ResponseAnsweExam>
	 */
	@PostMapping(value = "/answer-exam", produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseAnsweExam> answerExam(@Valid @RequestBody RqAnswerExam rqAnswerExam) {
		log.info("Inicia contestacion de examen");
		ResponseAnsweExam responseAnsweExam = new ResponseAnsweExam();
		responseAnsweExam.setScore(examService.answerExam(rqAnswerExam));
		
		responseAnsweExam.setStatus(new Status());
		responseAnsweExam.getStatus().setCode("OK");
		responseAnsweExam.getStatus().setMessage("El examen se contesto de manera correcta");
		
		log.info("Fin contestacion de examen");
		return new ResponseEntity<>(responseAnsweExam, HttpStatus.OK);
	}
}
