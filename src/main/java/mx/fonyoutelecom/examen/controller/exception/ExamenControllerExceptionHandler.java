package mx.fonyoutelecom.examen.controller.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import lombok.extern.slf4j.Slf4j;
import mx.fonyoutelecom.examen.model.RpBase;
import mx.fonyoutelecom.examen.model.Status;

/**
 * Controlador que se encarga de manejar la excepciones para regresar un error controlado
 * 
 * @author IAV
 *
 */
@Slf4j
@ControllerAdvice
public class ExamenControllerExceptionHandler {

	/**
	 * Metodo que se encarga de manejar las excepciones genericas
	 * 
	 * @param ex exception
	 * @return ResponseEntity<RpBase>
	 */
	@ExceptionHandler(Exception.class)
	public ResponseEntity<RpBase> handleException(Exception ex) {
		log.error("Ocurrio un error en el servicio", ex);
		RpBase resp = new RpBase();
		resp.setStatus(new Status());
		
		resp.getStatus().setCode("ERR99");
		resp.getStatus().setMessage("Ocurrio un error en el servicio " + ex.getMessage());
		return new ResponseEntity<>(resp, HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
