package mx.fonyoutelecom.examen.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Answers implements Serializable {

	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = -5757907145485970773L;

	@JsonProperty("id_answer")
	private int idAnswer;
	@JsonProperty("id_option")
	private int idOption;
	@JsonProperty("id_question")
	private int idQuestion;
	@JsonProperty("id_exam")
	private int idExam;
	@JsonProperty("id_assignment")
	private int idAssignment;
	@JsonProperty("id_student")
	private int idStudent;
}
