package mx.fonyoutelecom.examen.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "answers")
public class AnswersEntity implements Serializable {

	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 6169201736744705231L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_answer")
	private int idAnswer;
	
	@Column(name = "id_question")
	private int idQuestion;
	@Column(name = "id_exam")
	private int idExam;
	@Column(name = "id_student")
	private int idStudent;
	@Column(name = "id_option")
	private int idOption;
	@Column(name = "id_assignment")
	private int idAssignment;
	
}
