package mx.fonyoutelecom.examen.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConsultaAssignResponse extends RpBase{

	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = -5967679720951225786L;

	List<ExamAssignmentEntity> assing;
}
