package mx.fonyoutelecom.examen.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConsultaExamResponse extends RpBase {

	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 5272758880795794031L;

	private List<Exam> exams;
}
