package mx.fonyoutelecom.examen.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConsultaStudentsResponse extends RpBase {

	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = -2696469337437004471L;

	private List<Student> students;
}
