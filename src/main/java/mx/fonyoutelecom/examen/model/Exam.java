package mx.fonyoutelecom.examen.model;

import java.io.Serializable;
import java.util.List;


import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Exam implements Serializable {

	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = -6834349582947956624L;
	
	@JsonProperty("id_exam")
	private int	idExam;
	private String title;
	private String description;
	
	private List<Question> questions;

	@Override
	public String toString() {
		return "Exam [title=" + title + ", descripcion=" + description + ", questions=" + questions + "]";
	}
}
