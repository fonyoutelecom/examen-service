package mx.fonyoutelecom.examen.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExamAssignment implements Serializable {

	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = -4410743618755238273L;

	@Id
	@JsonProperty("id_assignment")
	private int idAssignment;
	
	@JsonProperty("id_student")
	private int idStudent;
	@JsonProperty("id_exam")
	private int idExam;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
	private Date date;
	private String hour;
	@JsonProperty("time_zone")
	private String timeZone;
	
	private int score;
}
