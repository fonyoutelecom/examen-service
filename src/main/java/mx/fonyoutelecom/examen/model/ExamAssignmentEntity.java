package mx.fonyoutelecom.examen.model;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "exam_assignment")
public class ExamAssignmentEntity implements Serializable {

	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = -4410743618755238273L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_assignment")
	private int idAssignment;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
	private Date date;
	private String hour;
	@Column(name = "time_zone")
	private String timeZone;
	
	private double score;
	
	@ManyToOne
	@JoinColumn(name = "id_exam")
	private ExamEntity exam;
	
	@ManyToOne
	@JoinColumn(name = "id_student")
	private Student student;
}

