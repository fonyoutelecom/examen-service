package mx.fonyoutelecom.examen.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "exams")
public class ExamEntity implements Serializable {

	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = 7862241116511415156L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_exam")
	private int idExam;
	private String title;
	private String description;
	
	@OneToMany(mappedBy = "idExam")
	private List<QuestionEntity> questions;
	
}
