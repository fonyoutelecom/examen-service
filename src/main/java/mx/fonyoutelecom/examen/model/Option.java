package mx.fonyoutelecom.examen.model;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Option implements Serializable {

	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = -1065520268969648752L;

	@NotNull(message = "El campo option no debe de ser nulo")
	@NotEmpty(message = "El campo option no debe de ser vacio")
	private String option;
	
	@JsonProperty("is_correct")
	private boolean isCorrect;

	/**
	 * (no-javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Options [option=" + option + ", isCorrect=" + isCorrect + "]";
	}
}
