package mx.fonyoutelecom.examen.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "options_question")
public class OptionEntity implements Serializable {

	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 6910013129479146883L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_option")
	private int idOption;
	@Column(name = "id_question")
	private int idQuestion;
	@Column(name = "id_exam")
	private int idExam;
	private String option;
	@Column(name = "is_correct")
	private boolean isCorrect;
}
