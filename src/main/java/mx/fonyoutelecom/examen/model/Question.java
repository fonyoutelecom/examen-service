package mx.fonyoutelecom.examen.model;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Question implements Serializable {
	
	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 8799231038736153733L;
	
	@NotNull(message = "El campo question no debe de ser nulo")
	@NotEmpty(message = "El campo question no debe de ser vacio")
	private String question;
	
	@NotNull(message = "El campo score no debe de ser nulo")
	private double score;
	
	@Valid
	@NotNull(message = "El campo option1 no debe de ser nulo")
	private Option option1;
	
	@Valid
	@NotNull(message = "El campo option1 no debe de ser nulo")
	private Option option2;
	
	@Valid
	@NotNull(message = "El campo option1 no debe de ser nulo")
	private Option option3;
	
	@Valid
	@NotNull(message = "El campo option1 no debe de ser nulo")
	private Option option4;
	
	/**
	 * (no-javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Question [question=" + question + ", score=" + score + ", option1=" + option1 + ", option2=" + option2
				+ ", option3=" + option3 + ", option4=" + option4 + "]";
	}
}
