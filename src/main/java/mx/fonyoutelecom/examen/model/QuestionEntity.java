package mx.fonyoutelecom.examen.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "questions")
public class QuestionEntity implements Serializable {

	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 6622773866770664280L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_question")
	private int idQuestion;
	@Column(name = "id_exam")
	private int idExam;
	private String question;
	private double score;
	
	@OneToMany(mappedBy = "idQuestion")
	private List<OptionEntity> options;
}
