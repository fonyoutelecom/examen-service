package mx.fonyoutelecom.examen.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseAnsweExam extends RpBase {

	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = -5330578637629936385L;

	private double score;
}
