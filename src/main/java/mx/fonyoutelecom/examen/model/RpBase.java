package mx.fonyoutelecom.examen.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RpBase implements Serializable {

	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = -6781217506182049345L;
	
	private Status status;

}
