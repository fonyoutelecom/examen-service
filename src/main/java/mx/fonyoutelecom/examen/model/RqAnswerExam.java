package mx.fonyoutelecom.examen.model;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RqAnswerExam implements Serializable {

	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = -8806859576903733852L;

	@NotNull
	@JsonProperty("id_assignment")
	private int idAssignment;
	
	@NotNull
	@NotEmpty
	private List<Answers> answers;
}
