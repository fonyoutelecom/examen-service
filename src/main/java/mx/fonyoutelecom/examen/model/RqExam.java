package mx.fonyoutelecom.examen.model;

import java.io.Serializable;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RqExam implements Serializable {

	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = 4763626420894631086L;
	
	@NotEmpty(message = "El campo title no debe de ser vacio")
	@NotNull(message = "El campo title no debe de ser nulo")
	private String title;
	private String descripcion;
	
	@Valid
	@NotEmpty(message = "El campo questions no debe de ser vacio")
	@NotNull(message = "El campo questions no debe de ser nulo")
	private List<Question> questions;
	
	/**
	 * (no-javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RqExam [title=" + title + ", descripcion=" + descripcion + ", questions=" + questions + "]";
	}
}
