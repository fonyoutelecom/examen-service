package mx.fonyoutelecom.examen.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Status implements Serializable {

	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = -3451233265877493581L;

	private String code;
	private String message;
	
	/**
	 * (no-javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Status [code=" + code + ", message=" + message + "]";
	}
}
