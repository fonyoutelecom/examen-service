package mx.fonyoutelecom.examen.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name ="students")
public class Student implements Serializable {

	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = -6862785105340887536L;

	@Id
	@JsonProperty("id_student")
	private int idStudent;
	
	@NotNull
	@NotEmpty
	private String name;
	
	@NotNull
	private int age;
	
	@NotNull
	@NotEmpty
	private String city;
	
	@NotNull
	@NotEmpty
	@JsonProperty("time_zone")
	private String timeZone;
}
