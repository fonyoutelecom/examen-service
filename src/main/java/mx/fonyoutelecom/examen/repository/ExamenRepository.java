package mx.fonyoutelecom.examen.repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import lombok.extern.slf4j.Slf4j;
import mx.fonyoutelecom.examen.model.Exam;
import mx.fonyoutelecom.examen.model.Option;
import mx.fonyoutelecom.examen.model.Question;
import mx.fonyoutelecom.examen.model.RqExam;

/**
 * Clase que implementa los metodos para la creacion de un examen
 * 
 * @author IAV
 *
 */
@Slf4j
@Repository
public class ExamenRepository implements IExamenRepository {

	private static final String INSERT_EXAM = "INSERT INTO `exams` (`title`, `description`, `created_at`) VALUES (?, ?, current_timestamp())";
	
	private static final String INSERT_QUESTION = "INSERT INTO `questions` (`question`, `score`, `created_at`, `id_exam`) VALUES (?, ?, current_timestamp(), ?)";
	
	private static final String INSERT_OPTION = "INSERT INTO `options_question` (`option`, `is_correct`, `created_at`, `id_question`, `id_exam`) VALUES (?, ?, current_timestamp(), ?, ?)";
	
	private static final String SELECT_EXAMS = "SELECT * FROM `exams`";
	
	/** Inyeccion de dependecia del jdbcTemplate */
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	/**
	 * (no-javadoc)
	 * @see mx.fonyoutelecom.examen.repository.IExamenRepository#createExam(mx.fonyoutelecom.examen.model.RqExam)
	 */
	@Override
	public int createExam(RqExam rqExam) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		
		jdbcTemplate.update(connection -> {
			PreparedStatement ps = connection
			          .prepareStatement(INSERT_EXAM, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, rqExam.getTitle());
			ps.setString(2, rqExam.getDescripcion());
			return ps;
		}, keyHolder);

		return keyHolder.getKey().intValue();
	}

	/**
	 * (no-javadoc)
	 * @see mx.fonyoutelecom.examen.repository.IExamenRepository#createQuestion(mx.fonyoutelecom.examen.model.Question, int)
	 */
	@Override
	public int createQuestion(Question question, int idExam) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		
		jdbcTemplate.update(connection -> {
			PreparedStatement ps = connection
			          .prepareStatement(INSERT_QUESTION, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, question.getQuestion());
			ps.setDouble(2, question.getScore());
			ps.setDouble(3, idExam);
			return ps;
		}, keyHolder);
		int id = keyHolder.getKey().intValue();
		
		saveOptions(question.getOption1(), id, idExam);
		saveOptions(question.getOption2(), id, idExam);
		saveOptions(question.getOption3(), id, idExam);
		saveOptions(question.getOption4(), id, idExam);
		
		return id;
	}
	
	/**
	 * Metodo que se encarga de guardar las opciones
	 * 
	 * @param option opcion
	 * @param idQuestion id de la pregunta
	 * @param idExam id del examen
	 */
	private void saveOptions(Option option, int idQuestion, int idExam) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		
		jdbcTemplate.update(connection -> {
			PreparedStatement ps = connection
			          .prepareStatement(INSERT_OPTION, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, option.getOption());
			ps.setBoolean(2, option.isCorrect());
			ps.setInt(3, idQuestion);
			ps.setInt(4, idExam);
			return ps;
		}, keyHolder);
		log.info("ID optiong: " + keyHolder.getKey());
	}

	/**
	 * (no-javadoc)
	 * @see mx.fonyoutelecom.examen.repository.IExamenRepository#getAllExam()
	 */
	@Override
	public List<Exam> getAllExam() {
		
		List<Exam> exams = jdbcTemplate.query(SELECT_EXAMS, new BeanPropertyRowMapper(Exam.class));
		
		
		return exams;
	}
	
	

}
