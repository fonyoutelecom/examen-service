package mx.fonyoutelecom.examen.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import mx.fonyoutelecom.examen.model.AnswersEntity;

/**
 * Interfaz que se utiliza para la gestion de las operaciones hacia la entidad answers
 * 
 * @author IAV
 *
 */
@Repository
public interface IAnswersRepository extends CrudRepository <AnswersEntity, Integer> {

}