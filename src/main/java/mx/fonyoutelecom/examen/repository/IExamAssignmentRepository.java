package mx.fonyoutelecom.examen.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import mx.fonyoutelecom.examen.model.ExamAssignmentEntity;

/**
 * Interfaz que se encarga de gestionar los operaciones hacia la entidad ExamAssignmentEntity
 * 
 * @author IAV
 *
 */
@Repository
public interface IExamAssignmentRepository extends CrudRepository <ExamAssignmentEntity, Integer> {
	
	/**
	 * Metodo que se encarga de consultar las asignaciones a partir del id_student, date, hour
	 * 
	 * @param idStudent id del estudiante
	 * @param date fecha
	 * @param hour hora
	 * @return List<ExamAssignmentEntity> lista de examenes
	 */
	@Query("SELECT e FROM exam_assignment e WHERE id_student = ?1 and date = DATE_FORMAT(?2,'%Y-%m-%d') and hour < ?3")
	List<ExamAssignmentEntity> findAssignment(int idStudent, Date date, String hour);
}
