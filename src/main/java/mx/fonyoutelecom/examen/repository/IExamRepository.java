package mx.fonyoutelecom.examen.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import mx.fonyoutelecom.examen.model.ExamEntity;

/**
 * Interfaz que se encarga de gestionar los metodos para la entidad ExamEntity
 * 
 * @author IAV
 *
 */
@Repository
public interface IExamRepository extends CrudRepository <ExamEntity, Integer> {

}
