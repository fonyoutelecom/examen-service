package mx.fonyoutelecom.examen.repository;

import java.util.List;

import mx.fonyoutelecom.examen.model.Exam;
import mx.fonyoutelecom.examen.model.Question;
import mx.fonyoutelecom.examen.model.RqExam;

/**
 * Interfaz que contiene los metodos para la creacion de un examen
 * 
 * @author IAV
 *
 */
public interface IExamenRepository {

	/**
	 * Metodo que se encarga de crear un examen
	 * 
	 * @param rqExam datos del examen
	 * @return int id del examen
	 */
	int createExam(RqExam rqExam);
	
	/**
	 * Metodo que se encarga de guardar las preguntas del examen
	 * 
	 * @param question preguntas
	 * @param idExam id del examen
	 * @return int id de la pregunta
	 */
	int createQuestion(Question question, int idExam);
	
	/**
	 * Metodo que se encarga de consultar todos los examenes
	 * 
	 * @return List<Exam> lista de examenes
	 */
	List<Exam> getAllExam();
}
