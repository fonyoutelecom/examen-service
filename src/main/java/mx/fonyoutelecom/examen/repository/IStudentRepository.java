package mx.fonyoutelecom.examen.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import mx.fonyoutelecom.examen.model.Student;

/**
 * Interfaz que se encarga de gestionar los metodos para la entidad Student
 * 
 * @author IAV
 *
 */
@Repository
public interface IStudentRepository extends CrudRepository <Student, Integer> {

}
