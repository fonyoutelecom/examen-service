package mx.fonyoutelecom.examen.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TimeZone;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import mx.fonyoutelecom.examen.model.Answers;
import mx.fonyoutelecom.examen.model.AnswersEntity;
import mx.fonyoutelecom.examen.model.ConsultaStudentsResponse;
import mx.fonyoutelecom.examen.model.Exam;
import mx.fonyoutelecom.examen.model.ExamAssignment;
import mx.fonyoutelecom.examen.model.ExamAssignmentEntity;
import mx.fonyoutelecom.examen.model.ExamEntity;
import mx.fonyoutelecom.examen.model.RqAnswerExam;
import mx.fonyoutelecom.examen.model.RqExam;
import mx.fonyoutelecom.examen.model.Student;
import mx.fonyoutelecom.examen.repository.IAnswersRepository;
import mx.fonyoutelecom.examen.repository.IExamAssignmentRepository;
import mx.fonyoutelecom.examen.repository.IExamRepository;
import mx.fonyoutelecom.examen.repository.IExamenRepository;
import mx.fonyoutelecom.examen.repository.IStudentRepository;

/**
 * Clase que implementa los metodos para las operaciones de los examenes y contiene la logica de negocio para las operaciones
 * 
 * @author IAV
 *
 */
@Slf4j
@Service
public class ExamenService implements IExamenService {
	
	/** Inyeccion de dependencia examenRepository */
	@Autowired
	private IExamenRepository examenRepository;
	
	/** Inyeccion de dependencia studentRepository */
	@Autowired
	private IStudentRepository studentRepository;
	
	/** Inyeccion de dependencia examAssignmentRepository */
	@Autowired
	private IExamAssignmentRepository examAssignmentRepository;
	
	/** Inyeccion de dependencia examRepository */
	@Autowired
	private IExamRepository examRepository;
	
	/** Inyeccion de dependencia answersRepository */
	@Autowired
	private IAnswersRepository answersRepository;
	
	/**
	 * (no-javadoc)
	 * @see mx.fonyoutelecom.examen.service.IExamenService#createExam(mx.fonyoutelecom.examen.model.RqExam)
	 */
	@Override
	public void createExam(RqExam rqExam) {
		/** Se crea el examen y se obtiene el ID */
		int idExam = examenRepository.createExam(rqExam);
		/** Se guardan las preguntas correspondientes al examen */
		rqExam.getQuestions().stream().forEach(question ->{
			examenRepository.createQuestion(question, idExam);
		});
	}

	/**
	 * (no-javadoc)
	 * @see mx.fonyoutelecom.examen.service.IExamenService#getAllExam()
	 */
	@Override
	public List<Exam> getAllExam() {
		return examenRepository.getAllExam();
	}

	/**
	 * (no-javadoc)
	 * @see mx.fonyoutelecom.examen.service.IExamenService#createStudent(mx.fonyoutelecom.examen.model.Student)
	 */
	@Override
	public void createStudent(Student rqStudent) {
		studentRepository.save(rqStudent);
	}

	/**
	 * (no-javadoc)
	 * @see mx.fonyoutelecom.examen.service.IExamenService#getStudents()
	 */
	@Override
	public ConsultaStudentsResponse getStudents() {
		ConsultaStudentsResponse result = new ConsultaStudentsResponse();

		result.setStudents((List<Student>) studentRepository.findAll());;

		return result;
	}

	/**
	 * (no-javadoc)
	 * @see mx.fonyoutelecom.examen.service.IExamenService#examAssignment(mx.fonyoutelecom.examen.model.ExamAssignment)
	 */
	@Override
	public void examAssignment(ExamAssignment examAssignment) {
		Optional<Student> student = studentRepository.findById(examAssignment.getIdStudent());
		Optional<ExamEntity> exam = examRepository.findById(examAssignment.getIdExam());
		ExamAssignmentEntity examAssignmentEntity = new ExamAssignmentEntity();
		examAssignmentEntity.setDate(examAssignment.getDate());
		examAssignmentEntity.setHour(examAssignment.getHour());
		/** Si se logro consultar el estudiante se asignan sus datos */
		if(student.isPresent()) {
			examAssignmentEntity.setTimeZone(student.get().getTimeZone());
			examAssignmentEntity.setStudent(student.get());
		}
		/** Si se logro consultar el examen se asignan sus datos */
		if(exam.isPresent()) {
			examAssignmentEntity.setExam(exam.get());
		}
		
		examAssignmentRepository.save(examAssignmentEntity);
	}

	/**
	 * (no-javadoc)
	 * @see mx.fonyoutelecom.examen.service.IExamenService#getAssignmentByStudent(int)
	 */
	@Override
	public List<ExamAssignmentEntity> getAssignmentByStudent(int idStudent) {
		Optional<Student> student = studentRepository.findById(idStudent);
		/** Se valida que se haya recuperado el estudiante */
		if(student.isPresent()) {
			TimeZone timeZone = TimeZone.getTimeZone(student.get().getTimeZone());
	        SimpleDateFormat formatterWithTimeZone = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
	        formatterWithTimeZone.setTimeZone(timeZone);
	        String sDate = formatterWithTimeZone.format(new Date());
	        
	        List<ExamAssignmentEntity> assing;
			try {
				assing = examAssignmentRepository.findAssignment(idStudent, new SimpleDateFormat("yyyy-MM-dd").parse(sDate.substring(0, 10)),sDate.substring(11));
				
				if(assing != null && !assing.isEmpty()) {
					return assing;
				}
			} catch (ParseException e1) {
				log.error("Error al parsear la fecha", e1);
			}
		}
		
		return null;
	}

	/**
	 * (no-javadoc)
	 * @see mx.fonyoutelecom.examen.service.IExamenService#answerExam(mx.fonyoutelecom.examen.model.RqAnswerExam)
	 */
	@Override
	public double answerExam(RqAnswerExam rqAnswerExam) {
		Optional<ExamAssignmentEntity> assing = examAssignmentRepository.findById(rqAnswerExam.getIdAssignment());
		/** Se valida que se haya recuperado la assignacion */
		if(assing.isPresent()) {
			
			rqAnswerExam.getAnswers().stream().forEach(a -> {
				AnswersEntity answersEntity = new AnswersEntity();
				answersEntity.setIdAssignment(rqAnswerExam.getIdAssignment());
				answersEntity.setIdOption(a.getIdOption());
				answersEntity.setIdExam(assing.get().getExam().getIdExam());
				answersEntity.setIdStudent(assing.get().getStudent().getIdStudent());
				answersEntity.setIdQuestion(a.getIdQuestion());
				
				answersRepository.save(answersEntity);
			});
			
			Map<Integer, Double> options = new HashMap<>();
			assing.get().getExam().getQuestions().stream().forEach(q -> {
				options.put(q.getOptions().stream().filter(o -> o.isCorrect()).collect(Collectors.toList()).get(0).getIdOption(), q.getScore()); 
			});
			
			double score = 0;
			for(Answers a: rqAnswerExam.getAnswers()) {
				if(options.containsKey(a.getIdOption())) {
					score += options.get(a.getIdOption());
				}
			}
			
			assing.get().setScore(score);
			examAssignmentRepository.save(assing.get());
			
			return score;
		}
		
		return 0;
	}

}
