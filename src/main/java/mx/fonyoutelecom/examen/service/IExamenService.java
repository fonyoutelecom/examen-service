package mx.fonyoutelecom.examen.service;

import java.util.List;

import mx.fonyoutelecom.examen.model.ConsultaStudentsResponse;
import mx.fonyoutelecom.examen.model.Exam;
import mx.fonyoutelecom.examen.model.ExamAssignment;
import mx.fonyoutelecom.examen.model.ExamAssignmentEntity;
import mx.fonyoutelecom.examen.model.RqAnswerExam;
import mx.fonyoutelecom.examen.model.RqExam;
import mx.fonyoutelecom.examen.model.Student;

/**
 * Interfaz que contiene los metodos para las operaciones del examen
 * 
 * @author IAV
 *
 */
public interface IExamenService {

	/**
	 * Metodo que se encarga de crear un examen
	 * 
	 * @param rqExam datos del examen a crear
	 */
	void createExam(RqExam rqExam);
	
	/**
	 * Metodo que se encarga de consultar todos los examenes creados
	 * 
	 * @return List<Exam>
	 */
	List<Exam> getAllExam();
	
	/**
	 * Metodo que se encarga de crear un estudiante
	 * 
	 * @param rqStudent datos del estudiante a crear
	 */
	void createStudent(Student rqStudent);
	
	/**
	 * Metodo que se encarga de consultar los estudiantes creados
	 * 
	 * @return ConsultaStudentsResponse
	 */
	ConsultaStudentsResponse getStudents();
	
	/**
	 * Metodo que se encarga de hacer la asignacion de un examen
	 * 
	 * @param examAssignment datos de la asignacion
	 */
	void examAssignment(ExamAssignment examAssignment);
	
	/**
	 * Metodo que se encarga de consultar las asignaciones de un estudiante a partir de su id
	 * 
	 * @param idStudent id del estudiante
	 * @return List<ExamAssignmentEntity> lista de asignaciones
	 */
	List<ExamAssignmentEntity> getAssignmentByStudent(int idStudent);
	
	/**
	 * Metodo que se encarga de guardar las respuestas de un examen
	 * 
	 * @param rqAnswerExam respuestas del examen
	 * @return double calificacion del examen
	 */
	double answerExam(RqAnswerExam rqAnswerExam);
}
