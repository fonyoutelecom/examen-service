package mx.fonyoutelecom.examen.utils;

import java.time.ZoneId;
import java.time.zone.ZoneRulesException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public final class UtilsExam {
	
	private UtilsExam() {
		// Constructor privado para evitar que la clase sea instanciada
	}
	
	public static boolean isTimeZoneValid(String timeZone) {
		try {
			return ZoneId.of(timeZone) != null;
		} catch (ZoneRulesException e) {
			log.error("Time zone invalido");
		}
		
		return false;
	}

}
