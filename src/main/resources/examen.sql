-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-07-2022 a las 12:26:35
-- Versión del servidor: 10.4.8-MariaDB
-- Versión de PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `examen`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `answers`
--

CREATE TABLE `answers` (
  `id_answer` int(11) NOT NULL,
  `id_option` int(11) NOT NULL,
  `id_question` int(11) NOT NULL,
  `id_exam` int(11) NOT NULL,
  `id_assignment` int(11) NOT NULL,
  `id_student` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `answers`
--

INSERT INTO `answers` (`id_answer`, `id_option`, `id_question`, `id_exam`, `id_assignment`, `id_student`) VALUES
(2, 26, 8, 10, 15, 3),
(3, 30, 9, 10, 15, 3),
(4, 26, 8, 10, 15, 3),
(5, 30, 9, 10, 15, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `exams`
--

CREATE TABLE `exams` (
  `id_exam` int(11) NOT NULL,
  `title` varchar(80) NOT NULL COMMENT 'Titulo del examen',
  `description` varchar(200) NOT NULL COMMENT 'Descripcion del examen',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'Fecha de creacion del examen'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `exams`
--

INSERT INTO `exams` (`id_exam`, `title`, `description`, `created_at`) VALUES
(1, 'Examen 5', 'Examen de prueba', '2022-07-07 18:11:13'),
(2, 'Examen 5', 'Examen de prueba', '2022-07-07 18:16:07'),
(3, 'Examen 5', 'Examen de prueba', '2022-07-07 18:17:22'),
(4, 'Examen 5', 'Examen de prueba', '2022-07-07 18:24:28'),
(5, 'Examen 5', 'Examen de prueba', '2022-07-07 19:11:59'),
(6, 'Examen 5', 'Examen de prueba', '2022-07-07 19:12:11'),
(7, 'Examen 5', 'Examen de prueba', '2022-07-07 21:11:05'),
(8, 'Examen 5', 'Examen de prueba', '2022-07-07 21:12:56'),
(9, 'Examen 5', 'Examen de prueba', '2022-07-13 08:45:51'),
(10, 'Examen 6', 'Examen de prueba', '2022-07-13 09:02:22');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `exams_questions`
--

CREATE TABLE `exams_questions` (
  `exams_id_exam` int(11) NOT NULL,
  `questions_id_question` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `exam_assignment`
--

CREATE TABLE `exam_assignment` (
  `id_assignment` int(11) NOT NULL,
  `date` date NOT NULL COMMENT 'fecha de asignacion del examen',
  `hour` varchar(20) NOT NULL,
  `time_zone` varchar(20) NOT NULL,
  `score` double NOT NULL,
  `id_student` int(11) NOT NULL,
  `id_exam` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `exam_assignment`
--

INSERT INTO `exam_assignment` (`id_assignment`, `date`, `hour`, `time_zone`, `score`, `id_student`, `id_exam`) VALUES
(15, '2022-07-13', '01:13:00 AM', 'America/Mexico_City', 50, 3, 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `options_question`
--

CREATE TABLE `options_question` (
  `id_option` int(11) NOT NULL,
  `option` varchar(200) NOT NULL,
  `is_correct` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `id_question` int(11) NOT NULL,
  `id_exam` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `options_question`
--

INSERT INTO `options_question` (`id_option`, `option`, `is_correct`, `created_at`, `id_question`, `id_exam`) VALUES
(1, 'java', 0, '2022-07-07 18:18:44', 1, 3),
(2, 'lenguaje de programacion', 1, '2022-07-07 18:24:28', 2, 4),
(3, 'Receta de cocina', 0, '2022-07-07 18:24:28', 2, 4),
(4, 'Un framework', 0, '2022-07-07 18:24:28', 2, 4),
(5, 'Un lugar', 0, '2022-07-07 18:24:28', 2, 4),
(6, 'lenguaje de programacion', 1, '2022-07-07 19:11:59', 3, 5),
(7, 'Receta de cocina', 0, '2022-07-07 19:11:59', 3, 5),
(8, 'Un framework', 0, '2022-07-07 19:11:59', 3, 5),
(9, 'Un lugar', 0, '2022-07-07 19:11:59', 3, 5),
(10, 'lenguaje de programacion', 1, '2022-07-07 19:12:11', 4, 6),
(11, 'Receta de cocina', 0, '2022-07-07 19:12:11', 4, 6),
(12, 'Un framework', 0, '2022-07-07 19:12:11', 4, 6),
(13, 'Un lugar', 0, '2022-07-07 19:12:11', 4, 6),
(14, 'lenguaje de programacion', 1, '2022-07-07 21:11:05', 5, 7),
(15, 'Receta de cocina', 0, '2022-07-07 21:11:06', 5, 7),
(16, 'Un framework', 0, '2022-07-07 21:11:06', 5, 7),
(17, 'Un lugar', 0, '2022-07-07 21:11:06', 5, 7),
(18, 'lenguaje de programacion', 1, '2022-07-07 21:12:56', 6, 8),
(19, 'Receta de cocina', 0, '2022-07-07 21:12:56', 6, 8),
(20, 'Un framework', 0, '2022-07-07 21:12:56', 6, 8),
(21, 'Un lugar', 0, '2022-07-07 21:12:56', 6, 8),
(22, 'lenguaje de programacion', 1, '2022-07-13 08:45:51', 7, 9),
(23, 'Receta de cocina', 0, '2022-07-13 08:45:51', 7, 9),
(24, 'Un framework', 0, '2022-07-13 08:45:51', 7, 9),
(25, 'Un lugar', 0, '2022-07-13 08:45:51', 7, 9),
(26, 'lenguaje de programacion', 1, '2022-07-13 09:02:22', 8, 10),
(27, 'Receta de cocina', 0, '2022-07-13 09:02:22', 8, 10),
(28, 'Un framework', 0, '2022-07-13 09:02:22', 8, 10),
(29, 'Un lugar', 0, '2022-07-13 09:02:22', 8, 10),
(30, 'Clases', 0, '2022-07-13 09:02:22', 9, 10),
(31, 'Objetos', 1, '2022-07-13 09:02:22', 9, 10),
(32, 'Herencia', 0, '2022-07-13 09:02:22', 9, 10),
(33, 'Polimorfismo', 0, '2022-07-13 09:02:22', 9, 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `questions`
--

CREATE TABLE `questions` (
  `id_question` int(11) NOT NULL COMMENT 'Id de la pregunta',
  `question` varchar(150) NOT NULL COMMENT 'Pregunta',
  `score` double NOT NULL COMMENT 'Puntaje',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `id_exam` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `questions`
--

INSERT INTO `questions` (`id_question`, `question`, `score`, `created_at`, `id_exam`) VALUES
(1, '¿Que es java?', 100, '2022-07-07 18:17:22', 3),
(2, '¿Que es java?', 100, '2022-07-07 18:24:28', 4),
(3, '¿Que es java?', 100, '2022-07-07 19:11:59', 5),
(4, '¿Que es java?', 100, '2022-07-07 19:12:11', 6),
(5, '¿Que es java?', 100, '2022-07-07 21:11:05', 7),
(6, '¿Que es java?', 100, '2022-07-07 21:12:56', 8),
(7, '¿Que es java?', 10, '2022-07-13 08:45:51', 9),
(8, '¿Que es java?', 50, '2022-07-13 09:02:22', 10),
(9, 'Están conformados por datos o propiedades, por un comportamiento y poseen una identidad o identificación única respecto a otros objetos', 50, '2022-07-13 09:02:22', 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `students`
--

CREATE TABLE `students` (
  `id_student` int(11) NOT NULL COMMENT 'Id del estudiante',
  `name` varchar(60) NOT NULL COMMENT 'Nombre del estudiante',
  `age` int(11) NOT NULL COMMENT 'Edad del estudiante',
  `city` varchar(60) NOT NULL COMMENT 'Ciudad',
  `time_zone` varchar(60) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `students`
--

INSERT INTO `students` (`id_student`, `name`, `age`, `city`, `time_zone`, `created_at`) VALUES
(1, 'Ivan Acosta Vargas', 28, 'Mexico', 'America/Mexico_City', '2022-07-07 22:24:44'),
(2, 'Ivan Acosta Vargas', 28, 'Mexico', 'America/Mexico_City', '2022-07-07 22:28:15'),
(3, 'Ivan Acosta Vargas', 28, 'Mexico', 'America/Mexico_City', '2022-07-07 22:32:15'),
(4, 'Ivan Acosta Vargas', 28, 'Mexico', 'America/Mexico_City', '2022-07-13 08:47:57');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`id_answer`,`id_option`,`id_question`,`id_exam`,`id_assignment`,`id_student`),
  ADD KEY `Relationship5` (`id_option`,`id_question`),
  ADD KEY `Relationship6` (`id_assignment`,`id_student`,`id_exam`),
  ADD KEY `FK57hgoobsv4cne7ju9yky9rvj7` (`id_exam`),
  ADD KEY `FKa7obhh0b56p70kf3x0jsfdhog` (`id_question`),
  ADD KEY `FKh4887icegtesi0q9ev4igvlbf` (`id_student`);

--
-- Indices de la tabla `exams`
--
ALTER TABLE `exams`
  ADD PRIMARY KEY (`id_exam`);

--
-- Indices de la tabla `exams_questions`
--
ALTER TABLE `exams_questions`
  ADD UNIQUE KEY `UK_g5f2e09tfd8kjeh4de6vgmqle` (`questions_id_question`),
  ADD KEY `FKkyl2nac0x2wht19egfxwhi8e5` (`exams_id_exam`);

--
-- Indices de la tabla `exam_assignment`
--
ALTER TABLE `exam_assignment`
  ADD PRIMARY KEY (`id_assignment`,`id_student`,`id_exam`),
  ADD KEY `Relationship3` (`id_student`),
  ADD KEY `Relationship4` (`id_exam`);

--
-- Indices de la tabla `options_question`
--
ALTER TABLE `options_question`
  ADD PRIMARY KEY (`id_option`,`id_question`,`id_exam`),
  ADD KEY `Relationship2` (`id_question`,`id_exam`),
  ADD KEY `Relationship2_1` (`id_exam`);

--
-- Indices de la tabla `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id_question`,`id_exam`),
  ADD KEY `Relationship1` (`id_exam`);

--
-- Indices de la tabla `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id_student`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `answers`
--
ALTER TABLE `answers`
  MODIFY `id_answer` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `exams`
--
ALTER TABLE `exams`
  MODIFY `id_exam` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `exam_assignment`
--
ALTER TABLE `exam_assignment`
  MODIFY `id_assignment` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `options_question`
--
ALTER TABLE `options_question`
  MODIFY `id_option` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de la tabla `questions`
--
ALTER TABLE `questions`
  MODIFY `id_question` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id de la pregunta', AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `students`
--
ALTER TABLE `students`
  MODIFY `id_student` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id del estudiante', AUTO_INCREMENT=5;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `answers`
--
ALTER TABLE `answers`
  ADD CONSTRAINT `FK57hgoobsv4cne7ju9yky9rvj7` FOREIGN KEY (`id_exam`) REFERENCES `exams` (`id_exam`),
  ADD CONSTRAINT `FKa7obhh0b56p70kf3x0jsfdhog` FOREIGN KEY (`id_question`) REFERENCES `questions` (`id_question`),
  ADD CONSTRAINT `FKh4887icegtesi0q9ev4igvlbf` FOREIGN KEY (`id_student`) REFERENCES `students` (`id_student`),
  ADD CONSTRAINT `Relationship5` FOREIGN KEY (`id_option`,`id_question`) REFERENCES `options_question` (`id_option`, `id_question`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Relationship6` FOREIGN KEY (`id_assignment`,`id_student`,`id_exam`) REFERENCES `exam_assignment` (`id_assignment`, `id_student`, `id_exam`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `exams_questions`
--
ALTER TABLE `exams_questions`
  ADD CONSTRAINT `FKkyl2nac0x2wht19egfxwhi8e5` FOREIGN KEY (`exams_id_exam`) REFERENCES `exams` (`id_exam`),
  ADD CONSTRAINT `FKnhx8b69kpi2119prknn1sn2pt` FOREIGN KEY (`questions_id_question`) REFERENCES `questions` (`id_question`);

--
-- Filtros para la tabla `exam_assignment`
--
ALTER TABLE `exam_assignment`
  ADD CONSTRAINT `Relationship3` FOREIGN KEY (`id_student`) REFERENCES `students` (`id_student`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Relationship4` FOREIGN KEY (`id_exam`) REFERENCES `exams` (`id_exam`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `options_question`
--
ALTER TABLE `options_question`
  ADD CONSTRAINT `Relationship2` FOREIGN KEY (`id_question`,`id_exam`) REFERENCES `questions` (`id_question`, `id_exam`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Relationship2_1` FOREIGN KEY (`id_exam`) REFERENCES `exams` (`id_exam`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `questions`
--
ALTER TABLE `questions`
  ADD CONSTRAINT `Relationship1` FOREIGN KEY (`id_exam`) REFERENCES `exams` (`id_exam`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
